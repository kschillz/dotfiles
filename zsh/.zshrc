# Path to your oh-my-zsh installation.
export ZSH=/Users/kschiller/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME=bureau

plugins=(git colored-man colorize vagrant virtualenv pip python brew osx zsh-syntax-highlighting)

# User configuration

export PATH="/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# virtualenvwrapper
export WORKON_HOME="~/src/envs/"
source /usr/local/bin/virtualenvwrapper.sh

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias ec="emacsclient -a \"\" -n -c"
alias vim="nvim"

export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"
eval $(docker-machine env)
export PATH="~/bin:$PATH"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export VAULT_ADDR=http://qatools.cudaops.com:8200

export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"
